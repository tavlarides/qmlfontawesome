#include "fontawesome.h"

#include <QPainter>
#include <QFontDatabase>
#include <QFont>

#include <QDebug>

static int font_id;
static QString font_family;

void FontAwesome::initialize() {
  qmlRegisterType<FontAwesome>("FontAwesome", 1, 0, "FontAwesome");
  font_id = QFontDatabase::addApplicationFont(":/FontAwesome.otf");
  auto families = QFontDatabase::applicationFontFamilies(font_id);
  font_family = families.first();
}

FontAwesome::FontAwesome(QQuickItem *parent) : QQuickPaintedItem (parent) {
  m_timer.setSingleShot(false);
  connect(&m_timer, &QTimer::timeout, this, &FontAwesome::onTimer);
}

FontAwesome::IconID FontAwesome::icon() const {
  return m_icon;
}

void FontAwesome::updateMetrics() {
  QFont font(font_family);
  font.setPointSizeF(m_icon_size);

  QFontMetricsF metrics(font);

  /// in order to rotate nicely
  auto w = metrics.width(text());
  auto h = metrics.height();
  auto m = qMax(w, h);

  setImplicitWidth(m);
  setImplicitHeight(m);

  update();
}

void FontAwesome::updateTimer() {
  m_timer.stop();
  if (m_animated) {
    m_angle = 0.0;
    int interval = 1000 / m_fps;
    m_increment = 360.0 * interval / m_period * (m_clockwise ? 1 : -1);
    m_timer.setInterval(interval);
      m_timer.start();
  }
}

void FontAwesome::onTimer() {
  m_angle += m_increment;
  if (m_angle >= 360)
    m_angle = 0;
  update();
}

void FontAwesome::setIcon(FontAwesome::IconID icon) {
  if (m_icon != icon) {
    m_icon = icon;
    emit iconChanged();
    updateMetrics();
  }
}

qreal FontAwesome::iconSize() const {
  return m_icon_size;
}

void FontAwesome::setIconSize(const qreal &icon_size) {
  if (!qFuzzyCompare(m_icon_size, icon_size)) {
    m_icon_size = icon_size;
    emit iconSizeChanged();
    updateMetrics();
  }
}

bool FontAwesome::isAnimated() const {
  return m_animated;
}

void FontAwesome::setAnimated(bool on) {
  if (m_animated != on) {
    m_animated = on;
    emit animatedChanged();
    updateTimer();
  }
}

int FontAwesome::period() const {
  return m_period;
}

void FontAwesome::setPeriod(int period) {
  if (m_period != period) {
    m_period = period;
    emit periodChanged();
    updateTimer();
  }
}

int FontAwesome::fps() const {
  return m_fps;
}

void FontAwesome::setFps(int fps) {
  if (m_fps != fps) {
    m_fps = fps;
    emit fpsChanged();
    updateTimer();
  }
}


void FontAwesome::paint(QPainter *painter) {
    QFont font(font_family);
    font.setPointSizeF(m_icon_size);
    painter->setFont(font);
    painter->save();
    auto rect = boundingRect();
    if (m_animated) {
      auto center = boundingRect().center();
      painter->translate(center);
      painter->rotate(m_angle);
      rect.translate(-center);
    }
    painter->drawText(rect, Qt::AlignCenter, text());
    painter->restore();

//    qDebug() << m_period;
}
