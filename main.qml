import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.2

import FontAwesome 1.0

Window {
  id: theWindow

  property variant credit_cards: [
    FontAwesome.Cc_amex,
    FontAwesome.Cc_discover,
    FontAwesome.Cc_mastercard,
    FontAwesome.Cc_paypal,
    FontAwesome.Cc_stripe,
    FontAwesome.Cc_visa,
    FontAwesome.Google_wallet,
    FontAwesome.Paypal,
  ]

  visible: true
  width: 640
  height: 480
  title: qsTr("Hello World")

  Row {
    spacing: 20
    Button {
      text: "Busy"
      FontAwesome {

        animated: true
        icon: FontAwesome.Gear

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 5
      }
    }


    Rectangle {

      color: "salmon"

      implicitWidth: clock.implicitWidth + 12
      implicitHeight: clock.implicitHeight + 12

      radius: width / 2
      border.color: "red"

      FontAwesome {
        id: clock
        anchors.centerIn: parent

        icon: FontAwesome.Long_arrow_up

        animated: true
        clockwise: true
        period: 60000
        fps: 1
      }
    }

  }

  Row {
    id: row
    spacing: 10

    anchors.centerIn: parent

    Repeater {
      model: credit_cards
      FontAwesome { icon: modelData }
    }
  }

}
